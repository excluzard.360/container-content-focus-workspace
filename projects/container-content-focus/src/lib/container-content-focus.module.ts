import { NgModule } from '@angular/core';
import { ContainerContentFocusDirective } from './directives/container-content-focus.directive';
import { ContainerContentFocusService } from './services/container-content-focus.service';
import { BgBackDropComponent } from './components/bg-back-drop.component';



@NgModule({
  declarations: [ContainerContentFocusDirective, BgBackDropComponent],
  imports: [
  ],
  providers: [ContainerContentFocusService],
  entryComponents:[BgBackDropComponent],
  exports: [ContainerContentFocusDirective]
})
export class ContainerContentFocusModule { }
