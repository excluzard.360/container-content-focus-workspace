import { TestBed } from '@angular/core/testing';

import { ContainerContentFocusService } from './container-content-focus.service';

describe('ContainerContentFocusService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ContainerContentFocusService = TestBed.get(ContainerContentFocusService);
    expect(service).toBeTruthy();
  });
});
