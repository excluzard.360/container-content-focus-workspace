// import { Injectable } from '@angular/core';
import { Injectable, QueryList } from '@angular/core';
import { ContainerContentFocusDirective } from '../directives/container-content-focus.directive';
import { of, Observable, BehaviorSubject } from 'rxjs';
import {  map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class ContainerContentFocusService {

  constructor() { }

  // @ViewChildren(ContainerContentFocusDirective) containerContentFocus !: QueryList<ContainerContentFocusDirective>;
  containerContentFocus !: QueryList<ContainerContentFocusDirective>;
  serializedPanes: string = '';

  groubs  = {};

  initViewChildren(containerContentFocus: QueryList<ContainerContentFocusDirective>) {
    if (containerContentFocus) {
      this.containerContentFocus = containerContentFocus;

      this.calculateSerializedContainerContentFocus();
      this.containerContentFocus.changes.subscribe((r) => { this.calculateSerializedContainerContentFocus(); });

    this.containerContentFocus.changes.subscribe((r)=>{
      // console.log("r:", r);
      // r.last.toggle();
      this.generateGroup(r); 
    });

    containerContentFocus.last.focus()

    }
  }

  private generateGroup(datas : QueryList<ContainerContentFocusDirective>) { 
    let maps: Map<string, Array<ContainerContentFocusDirective>> = new Map<string, Array<ContainerContentFocusDirective>>();
    datas.map(data => {
      maps = this.pushData(maps, data.group, data);
    });
    
    Object.keys(maps).forEach(key => {
      this.groubSubject(key).next(maps[key]);
    });

  }

  private pushData(map: Map<string, Array<ContainerContentFocusDirective>>, key: string = "nonGroub", data: ContainerContentFocusDirective){
    // console.log("map:", map)
    if(!map[key]){
      map[key] = new Array<ContainerContentFocusDirective>();
    }
    map[key]['push'](data);
    return map;
  }

  private groubSubject(group : string) : BehaviorSubject<ContainerContentFocusDirective[]> {
    if (!Object.keys(this.groubs).find(tmp => tmp == group)) {
      this.groubs[group] = new BehaviorSubject<ContainerContentFocusDirective[]>([]);
    }
    return this.groubs[group];
  }

  guoup(group : string): Observable<ContainerContentFocusDirective[]> {
    return this.groubSubject(group).asObservable();
  }

  guoupLastItem(group : string): Observable<ContainerContentFocusDirective> {
    return this.groubSubject(group).asObservable().pipe(map(datas=>{
      let result = undefined;
      if(datas){
        result = datas[(datas.length-1)<0?0:datas.length-1]
      }
      return result;
    }));
  }

  guoupFirstItem(group : string): Observable<ContainerContentFocusDirective> {
    return this.groubSubject(group).asObservable().pipe(map(datas=>{
      let result = undefined;
      if(datas && datas.length>0){
        result = datas[0]
      }
      return result;
    }));
  }

  private calculateSerializedContainerContentFocus() {
    setTimeout(() => { this.serializedPanes = this.containerContentFocus.map(p => p.id).join(', '); }, 0);
  }

}
