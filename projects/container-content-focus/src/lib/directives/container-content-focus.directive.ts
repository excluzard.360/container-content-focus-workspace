import { Directive, ElementRef, OnDestroy, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { ContainerFocusOptions } from '../configs/container-focus.config';

@Directive({
  selector: '[containerContentFocus]',
  exportAs: 'content-focus'
})
export class ContainerContentFocusDirective implements OnDestroy, OnInit {

  @Input() id !: string;
  @Input() group !: string;
  @Input() color: string = "rgba(106, 208, 64, 0.66)";

  boxShadow_size: string = " 0px 0px 1.65rem";

  mContainerElement: HTMLElement;
  mConfigDefaults: ContainerFocusOptions;

  BACK_DROP_ID: string = 'container-content-focus-back-drop';

  protected _isFocus = false;

  constructor(public el: ElementRef) {
    this.mContainerElement = el.nativeElement;
  }

  ngOnDestroy() {
    this.mConfigDefaults = void 0;
    // this.removeBackDrop();
    this.delBackDrop();
  }

  ngOnInit(): void {
    this.mConfigDefaults = this.mConfigDefaults || this.getConfig();
    this.craeteBackDrop();

  }

  delBackDrop() {
    let backDrop = document.getElementById(this.BACK_DROP_ID);
    if(backDrop){
      backDrop.remove();
    }
  }

  craeteBackDrop() {

    // this.mContainerElement.style.position="absolute";
    this.mContainerElement.style.position = "relative";
    let doc = this.mContainerElement.ownerDocument;
    let backDrop = doc.getElementById(this.BACK_DROP_ID);

    if (!backDrop) {
      let node = doc.createElement("DIV");
      node.id = this.BACK_DROP_ID;
      // node.className = "presentationBackBrop show";
      // node.className = "presentationBackBrop";
      node = this.addStyleBackDrop(node);
      // node.style.cssText = ""
      // doc.getElementsByTagName("body")[0].appendChild(node);
      document.body.appendChild(node);
      // console.log("--------------------------->node:", node);
    }

  }

  addStyleBackDrop(node: HTMLElement): HTMLElement {
    // node.style.cssText="background-color: #0000006b;top: 0;position: fixed;left: 0;right: 0;width: 100%;bottom: 0%;transition: all .3s;opacity:0;z-index: -1;"
    node.style.backgroundColor = "#0000006b";
    node.style.position = "fixed";
    node.style.top = "0";
    node.style.left = "0";
    node.style.right = "0";
    node.style.width = "100%";
    node.style.bottom = "0%";
    node.style.transition = "all .3s";
    node.style.opacity = "0";
    node.style.zIndex = "-1";
    return node;
  }


  addBackDrop() {

    let doc = this.mContainerElement.ownerDocument;
    let backDrop = doc.getElementById(this.BACK_DROP_ID);

    if (backDrop) {
      backDrop.classList.add("show");
      backDrop.style.opacity = "1";
      backDrop.style.zIndex = "300";

    } else {
      this.craeteBackDrop();
    }
  }

  removeBackDrop() {

    let doc = this.mContainerElement.ownerDocument;
    let backDrop = doc.getElementById(this.BACK_DROP_ID);

    if (backDrop) {
      // backDrop.classList.remove("presentationBackBrop");
      // backDrop.classList.add("hide");
      backDrop.classList.remove("show");
      // backDrop.className = "";

      backDrop.style.opacity = "0";
      backDrop.style.zIndex = "-1";

    }

    // console.log("--------------------------->node:", backDrop);

  }

  @Input()
  set config(conf: ContainerFocusOptions) {
    this.mConfigDefaults = this.getConfig(conf);
  }

  protected getConfig(config?: ContainerFocusOptions): ContainerFocusOptions {
    return Object.assign({}, this.mConfigDefaults, config);
  }


  /* ---------feature-event-callback---------- */

  @Output()
  onFocus: EventEmitter<ContainerContentFocusDirective> = new EventEmitter<ContainerContentFocusDirective>();
  @Output()
  onUnFocus: EventEmitter<ContainerContentFocusDirective> = new EventEmitter<ContainerContentFocusDirective>();


  /* ---------feature-function---------- */


  // get isShown(): boolean {
  //   return this._isFocus;
  // }

  get isFocus(): boolean {
    return this._isFocus;
  }

  toggle(): void {
    return this._isFocus ? this.unFocus() : this.focus();
  }

  focus(): void {
    this.onFocus.emit(this);
    if (this._isFocus) {
      return;
    }
    this._isFocus = true;
    this.mContainerElement.style.zIndex = '359';
    this.mContainerElement.style.transform = 'translateY(-3px)';
    this.mContainerElement.style.transitionDuration = '300ms'
    // this.mContainerElement.style.boxShadow = 'rgba(106, 208, 64, 0.66) 0px 0px 1.65rem';
    this.mContainerElement.style.boxShadow = `${this.color}${this.boxShadow_size}`;

    this.addBackDrop();

  }

  unFocus(event?: Event): void {
    if (event) {
      event.preventDefault();
    }

    this.onUnFocus.emit(this);

    if (!this._isFocus) {
      return;
    }
    this._isFocus = false;
    this.removeBackDrop();
    this.mContainerElement.style.zIndex = 'auto';
    // this.mContainerElement.style.transform = 'translateY(+3px)';
    this.mContainerElement.style.transform = '';
    this.mContainerElement.style.transitionDuration = '300ms'
    this.mContainerElement.style.boxShadow = '';
  }

}
