import { Component, OnChanges, Input, ViewChild } from '@angular/core';

import postscribe from 'postscribe';

@Component({
  selector: 'git-code',
  template: `<div #gitCode></div>`,
  styleUrls: ['./git-code-component.component.scss']
})
export class GitCodeComponentComponent implements OnChanges {
  @Input() src;
  @ViewChild('gitCode') gist;

  constructor() { }

  ngOnChanges() {
    if (this.src) {
      postscribe(this.gist.nativeElement, `<script src="${this.src}"></script>`);
    }
  }
}