import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ContainerContentFocusModule } from 'container-content-focus';
// import { SimpleTestLibComponent } from './simple-test-lib/simple-test-lib.component';
import { InstallationComponent } from './installation/installation.component';
import { DocumentationComponent } from './documentation/documentation.component';
import { BlogOfMeComponent } from './blog-of-me/blog-of-me.component';
import { FormsModule } from '@angular/forms';
import { GitCodeComponentComponent } from './git-code-component/git-code-component.component';

@NgModule({
  declarations: [
    AppComponent,
    // SimpleTestLibComponent,
    InstallationComponent,
    DocumentationComponent,
    BlogOfMeComponent,
    GitCodeComponentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ContainerContentFocusModule,
    // GistModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
