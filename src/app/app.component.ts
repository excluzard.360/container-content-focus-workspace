import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  // templateUrl: './app.component copy.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'container-content-focus-workspace';
}
