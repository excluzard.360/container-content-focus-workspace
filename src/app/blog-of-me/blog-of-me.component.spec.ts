import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlogOfMeComponent } from './blog-of-me.component';

describe('BlogOfMeComponent', () => {
  let component: BlogOfMeComponent;
  let fixture: ComponentFixture<BlogOfMeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlogOfMeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlogOfMeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
