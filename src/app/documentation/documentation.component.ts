import { Component, OnInit, ViewChild } from '@angular/core';
import { ContainerContentFocusDirective } from 'container-content-focus';

@Component({
  selector: 'app-documentation',
  templateUrl: './documentation.component.html',
  styleUrls: ['./documentation.component.scss']
})
export class DocumentationComponent implements OnInit {

  @ViewChild('contentFocusMeCustom') contentFocusMeCustomRef: ContainerContentFocusDirective;
  @ViewChild('msgInput') msgInputRef: ContainerContentFocusDirective;

  constructor() { }

  items = ['focus-item-1', 'focus-item-2', 'focus-item-3']

  textCustomValue: string ='';
  constomItems = []
  // constomItems = ['focus-item-1', 'focus-item-2', 'focus-item-3']

  itemTable =  [
    {
    "index": 1,
    "index_start_at": 56,
    "integer": 30,
    "float": 14.9703,
    "name": "Patrick",
    "surname": "Carlton",
    "fullname": "Lisa Sanchez",
    "email": "ben@craig.ve",
    "bool": true
    },
    {
    "index": 2,
    "index_start_at": 57,
    "integer": 10,
    "float": 12.3694,
    "name": "Kristine",
    "surname": "Hanson",
    "fullname": "Nina Hu",
    "email": "penny@carlson.se",
    "bool": false
    },
    {
    "index": 3,
    "index_start_at": 58,
    "integer": 35,
    "float": 19.8816,
    "name": "Mary",
    "surname": "McFarland",
    "fullname": "Ross Diaz",
    "email": "lewis@chandler.uz",
    "bool": false
    },
    {
    "index": 4,
    "index_start_at": 59,
    "integer": 3,
    "float": 11.4184,
    "name": "Linda",
    "surname": "Melton",
    "fullname": "Audrey Jennings",
    "email": "tommy@hu.na",
    "bool": true
    },
    {
    "index": 5,
    "index_start_at": 60,
    "integer": 45,
    "float": 12.8424,
    "name": "Lester",
    "surname": "Bunn",
    "fullname": "Bill McIntosh",
    "email": "lynne@hughes.aq",
    "bool": false
    },
    {
    "index": 6,
    "index_start_at": 61,
    "integer": 34,
    "float": 14.6945,
    "name": "Audrey",
    "surname": "Walter",
    "fullname": "Clyde Boyette",
    "email": "kelly@lucas.mm",
    "bool": false
    },
    {
    "index": 7,
    "index_start_at": 62,
    "integer": 31,
    "float": 19.0259,
    "name": "Barbara",
    "surname": "Bradshaw",
    "fullname": "Shannon Davidson",
    "email": "katharine@hardy.ni",
    "bool": true
    },
    {
    "index": 8,
    "index_start_at": 63,
    "integer": 37,
    "float": 18.6238,
    "name": "Kathleen",
    "surname": "Banks",
    "fullname": "Colleen Brandt",
    "email": "paige@hanna.is",
    "bool": true
    },
    {
    "index": 9,
    "index_start_at": 64,
    "integer": 27,
    "float": 15.7591,
    "name": "Alison",
    "surname": "Carlton",
    "fullname": "Randy Johnston",
    "email": "alice@elmore.nr",
    "bool": false
    },
    {
    "index": 10,
    "index_start_at": 65,
    "integer": 0,
    "float": 18.9622,
    "name": "Randall",
    "surname": "Sherrill",
    "fullname": "Herbert Proctor",
    "email": "don@barefoot.tm",
    "bool": false
    }
    ];
    
    

  ngOnInit() {
  }

  postMsg(msg: string){
    this.constomItems.push(msg);
    this.textCustomValue = '';
    this.contentFocusMeCustomRef.unFocus();
  }

}
