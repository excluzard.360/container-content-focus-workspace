import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SimpleTestLibComponent } from './simple-test-lib.component';

describe('SimpleTestLibComponent', () => {
  let component: SimpleTestLibComponent;
  let fixture: ComponentFixture<SimpleTestLibComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimpleTestLibComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimpleTestLibComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
