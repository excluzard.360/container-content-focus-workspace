import { ContainerContentFocusService } from 'container-content-focus';
import { Component, OnInit, ViewChildren, QueryList, AfterViewInit, Input } from '@angular/core';
import { ContainerContentFocusDirective } from 'container-content-focus';
import { tap, take } from 'rxjs/operators';

@Component({
  selector: 'app-simple-test-lib',
  templateUrl: './simple-test-lib.component.html',
  styleUrls: ['./simple-test-lib.component.scss'],
  providers: [ContainerContentFocusService]
})
export class SimpleTestLibComponent implements OnInit, AfterViewInit {

  @ViewChildren(ContainerContentFocusDirective) containerContentFocus !: QueryList<ContainerContentFocusDirective>;

  _code_config :string = '@NgModule({imports: [ContainerContentFocusModule]})'

  @Input('ready')
  set ready(isReady: boolean) {
    // if (isReady) this.someCallbackMethod();
    console.log(isReady);
    
  }

  // items=["1","2","3","4"];
  items=[];

  // constructor(private containerContentFocusService: ContainerContentFocusService) { }
  constructor(private containerContentFocusService: ContainerContentFocusService) { }

  ngOnInit() {
    // this.containerContentFocusService.onInit(this.containerContentFocus);
    // this.containerContentFocusService.test();
    // this.ngAfterViewInit().
    
  }
  

  // -----------------

  ngAfterViewInit() {
    this.containerContentFocusService.initViewChildren(this.containerContentFocus);

    // this.containerContentFocus.changes.subscribe((r)=>{
    //   console.log("r:", r);
    //   r.last.toggle();
    // });
  }


  // -----------------

  testClick() {
    console.log(this.containerContentFocus);
    this.items.push((this.items.length+1)+'');
    // this.containerContentFocusService.test();

  }
  testClick2() {
    console.log('testClick2');
    console.log(this.containerContentFocus);
    this.containerContentFocusService.guoup('group1').subscribe(
      data=>{
        // console.log("data:", data);
      }
    )
    this.containerContentFocusService.guoupFirstItem('group1').subscribe(
      data=>{
        // console.log("data:", data);
      }
    )
    this.containerContentFocusService.guoupLastItem('group1')
    .pipe(
      take(1),
      tap(data=>{
        // data.toggle();
      })
    )
    .subscribe()

    this.containerContentFocus.changes.pipe(
      take(1),
      tap(data=>{
        data.last.toggle();
      })
    )
    .subscribe()

  }

  someCallbackMethod(){
    console.log("someCallbackMethod");
    
    // this.containerContentFocus.last.toggle();
  }

  

}
