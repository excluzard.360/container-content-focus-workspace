import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InstallationComponent } from './installation/installation.component';
import { DocumentationComponent } from './documentation/documentation.component';
import { BlogOfMeComponent } from './blog-of-me/blog-of-me.component';


const routes: Routes = [{
  path: '',
  component: InstallationComponent,
  pathMatch: 'full',
},{
  path: 'documentation',
  component: DocumentationComponent,
},{
  path: 'blog-of-me',
  component: BlogOfMeComponent,
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
